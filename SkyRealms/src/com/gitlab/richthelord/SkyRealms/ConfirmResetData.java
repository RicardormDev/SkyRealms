package com.gitlab.richthelord.SkyRealms;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class ConfirmResetData {
	private static ConfirmResetData instance = new ConfirmResetData();
	public static ConfirmResetData getInstance() {
		
		if(instance == null)
			instance = new ConfirmResetData();
		
		return instance;
	}
	private ArrayList<ConfirmReset> resets;
	
	public ConfirmResetData(){
		resets = new ArrayList<>();
	}
	
	public ArrayList<ConfirmReset> getResets(){
		return resets;
	}
	
	public ConfirmReset getReset(Player player){
		for(ConfirmReset cr : resets){
			if(cr.getPlayer() == player)
				return cr;
		}
		return null;
	}
	
	public ConfirmReset getReset(Inventory inv){
		for(ConfirmReset cr : resets){
			if(cr.getInv() == inv)
				return cr;
		}
		return null;
	}
	
	
}
