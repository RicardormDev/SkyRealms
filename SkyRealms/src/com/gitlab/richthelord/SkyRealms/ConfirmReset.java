package com.gitlab.richthelord.SkyRealms;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public abstract class ConfirmReset {
	
	private Inventory gui;
	private Player player;
	
	private boolean canDoIt;
	
	@SuppressWarnings("deprecation")
	public ConfirmReset(Player player){
		gui = Bukkit.getServer().createInventory(null, 9, "Reset Island | Sure?");
		
		ItemStack yes = new ItemStack(Material.STAINED_CLAY, 1, (short)0, (byte)5);
		ItemStack no = new ItemStack(Material.STAINED_CLAY, 1, (short)0, (byte)14);
		
		ItemMeta ym = yes.getItemMeta();
		ItemMeta nm = no.getItemMeta();
		
		ym.setDisplayName(ChatColor.GREEN + "Confirm");
		ym.setLore(Arrays.asList(ChatColor.GRAY + "This will reset your island"));
		
		nm.setDisplayName(ChatColor.RED + "Cancel");
		nm.setLore(Arrays.asList(ChatColor.GRAY + "Close this window ", ChatColor.GRAY + "and cancel the operation"));
		
		yes.setItemMeta(ym);
		no.setItemMeta(nm);
		
		gui.setItem(3, yes);
		gui.setItem(5, no);
		
		this.player = player;
		this.canDoIt = false;
		
		ConfirmResetData.getInstance().getResets().add(this);
	}
	
	public ConfirmReset open(){
		
		player.openInventory(gui);
		player.sendMessage(ChatColor.GREEN + "Are you sure?");
				
		return this;
	}
	
	public Player getPlayer(){
		return player;
	}
	
	public Inventory getInv(){
		return gui;
	}
	
	public boolean isCanDoIt() {
		return canDoIt;
	}

	public void setCanDoIt(boolean canDoIt) {
		this.canDoIt = canDoIt;
	}

	public abstract void action();
	
}
