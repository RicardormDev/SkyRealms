package com.gitlab.richthelord.SkyRealms;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.gitlab.richthelord.SkyRealms.IslandObject.Island;
import com.gitlab.richthelord.SkyRealms.IslandObject.IslandManager;
import com.gitlab.richthelord.SkyRealms.Shop.Menu;
import com.gitlab.richthelord.SkyRealms.Shop.ShopListener;
import com.gitlab.richthelord.SkyRealms.bob.MenusEvents;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

import net.milkbowl.vault.economy.Economy;
import net.minecraft.server.v1_9_R1.EntityInsentient;
import net.minecraft.server.v1_9_R1.EntityTypes;

public class Main extends JavaPlugin {
	public static final Logger log = Logger.getLogger("Minecraft"); 
	
	//ECONOMY
	public static Economy economy = null;
	
	public File file;
	public FileConfiguration config;
	//ShopConfigClass
	
	//SHOP
	public static Menu shop;
		
	@Override
	public void onEnable(){
		setupEconomy();
		
		file = new File("plugins/Gems/shop.yml");
		config = YamlConfiguration.loadConfiguration(file);
		
		getCommand("island").setExecutor(new Commands());
		getCommand("is").setExecutor(new Commands());
		getCommand("sell").setExecutor(new Commands());
		
		Bukkit.getServer().getPluginManager().registerEvents(new Events(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new MenusEvents(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ShopListener(this), this);
		
		IslandManager.getInstance().setup();
		IslandManager.getInstance().setup();
		
		shop = new Menu();
	}
	
	@Override
	public void onDisable(){
		for(Island is : IslandManager.getInstance().getIslands()){
			is.geTraveler().getStands()[0].remove();
			is.geTraveler().getStands()[1].remove();
		}
	}
	
	public static Plugin getPlugin(){
		return Bukkit.getServer().getPluginManager().getPlugin("SkyRealms");
	}
	
	public static WorldEditPlugin getWorldEdit() {
		return (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
	}
	
	public static WorldGuardPlugin getWorldGuard(){
		Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
		if(plugin == null || !(plugin instanceof WorldGuardPlugin)){
			return null;
		}
		return (WorldGuardPlugin) plugin;
	}
	
	   private boolean setupEconomy()
	    {
	        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
	        if (economyProvider != null) {
	            economy = economyProvider.getProvider();
	        }

	        return (economy != null);
	    }
	   
	   public Menu getShop(){
		   return shop;
	   }
	
	   public static void registerEntity(String name, int id, Class<? extends EntityInsentient> nmsClass, Class<? extends EntityInsentient> customClass){
	        try {
	     
	            List<Map<?, ?>> dataMap = new ArrayList<Map<?, ?>>();
	            for (Field f : EntityTypes.class.getDeclaredFields()){
	                if (f.getType().getSimpleName().equals(Map.class.getSimpleName())){
	                    f.setAccessible(true);
	                    dataMap.add((Map<?, ?>) f.get(null));
	                }
	            }
	     
	            if (dataMap.get(2).containsKey(id)){
	                dataMap.get(0).remove(name);
	                dataMap.get(2).remove(id);
	            }
	     
	            Method method = EntityTypes.class.getDeclaredMethod("a", Class.class, String.class, int.class);
	            method.setAccessible(true);
	            method.invoke(null, customClass, name, id);
	     
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	   
}
