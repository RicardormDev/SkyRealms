package com.gitlab.richthelord.SkyRealms;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftVillager;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gitlab.richthelord.SkyRealms.IslandObject.Island;
import com.gitlab.richthelord.SkyRealms.IslandObject.IslandManager;
import com.gitlab.richthelord.SkyRealms.bob.Menus.BobPrincipal;
import com.gitlab.richthelord.SkyRealms.bob.NPC.Bob;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_9_R1.EntityVillager;

public class Events implements Listener{

	@EventHandler
	public void invClick(InventoryClickEvent e){
		Player player = (Player)e.getWhoClicked();		
		ConfirmReset cr = ConfirmResetData.getInstance().getReset(player);
		if(!e.getInventory().getName().equals(cr.getInv().getName())){
			return;
		}
		e.setCancelled(true);
			
		if(e.getCurrentItem().getType() == Material.AIR)
			return;
			
		if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Confirm")){
			cr.action();
		} else if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Cancel")){
			player.closeInventory();
		}
	}
	
	@EventHandler
	public void clickNPC(PlayerInteractEntityEvent e){
		Entity en = e.getRightClicked();
		if(en instanceof Villager){
			EntityVillager znm = ((CraftVillager)en).getHandle();
			if(znm instanceof Bob){				
				((CraftVillager)en).setRemoveWhenFarAway(false);
				e.setCancelled(true);
				new BobPrincipal().build(e.getPlayer());
				e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_ITEM_PICKUP, 10, 1);
			}
		}
	}
	
	@EventHandler
	public void chatStar(AsyncPlayerChatEvent e){
		if(e.getFormat().contains("{prestige}")){
			e.setFormat(e.getFormat().replace("{prestige}", IslandManager.getInstance().getIsland(e.getPlayer().getName()).getPrestige().getChatFormat()));
		}
		
		if(e.getMessage().contains("{prestige}")){
			e.setMessage(e.getMessage().replace("{prestige}", IslandManager.getInstance().getIsland(e.getPlayer().getName()).getPrestige().getChatFormat()));
		}
	}
	
	@EventHandler
	public void portalBLock(BlockPlaceEvent e){
		if(!e.getItemInHand().hasItemMeta()) {
			return;
		}
		
		if(e.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "Island Portal | Invite people :D")){			
			Island is = IslandManager.getInstance().getIsland(e.getPlayer().getName());
			if(is != null){
				is.createPortal(e.getBlockPlaced().getLocation());				
				e.setCancelled(true);
				
				ItemStack item = e.getItemInHand();
				ItemMeta meta = item.getItemMeta();
				meta.setDisplayName(ChatColor.RED + "USED PORTAL");
				item.setItemMeta(meta);
				item.setType(Material.AIR);
			}
		}
	}
	
	@EventHandler
	public void armorstand(PlayerInteractAtEntityEvent e){
		if(e.getRightClicked() instanceof ArmorStand){
			String who = (e.getRightClicked().getCustomName().split("'")[0]);
			Island is = IslandManager.getInstance().getIsland(ChatColor.stripColor(who));
			if(is != null){
				e.setCancelled(true);
				e.getPlayer().teleport(is.getCenter());
				e.getPlayer().sendMessage(ChatColor.GREEN + "Commencing teleportation to the island of " + who);
			}
		}
	}
	
	@EventHandler
	public void armorstandDMG(EntityDamageByEntityEvent  e){
		if(e.getEntity() instanceof ArmorStand){
			String who = (e.getEntity().getCustomName().split("'")[0]);
			Island is = IslandManager.getInstance().getIsland(ChatColor.stripColor(who));
			if(is != null){
				e.setCancelled(true);
				e.getDamager().teleport(is.getCenter());
				e.getDamager().sendMessage(ChatColor.GREEN + "Commencing teleportation to the island of " + who);
			}
		}
	}
		
}
