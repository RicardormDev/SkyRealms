package com.gitlab.richthelord.SkyRealms.IslandObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftVillager;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

import com.gitlab.richthelord.SkyRealms.Main;
import com.gitlab.richthelord.SkyRealms.SettingsManager;
import com.gitlab.richthelord.SkyRealms.Utils.SchemUtils;
import com.gitlab.richthelord.SkyRealms.bob.NPC.Bob;
import com.gitlab.richthelord.SkyRealms.bob.NPC.CustomEntities;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.EntityVillager;

@SuppressWarnings("deprecation")
public class isOLD {

	private String uuid;
	private String player;
	
	private CuboidClipboard islandBlocks;
	private Location center;
	
	private Location spawnLoc;
	
	private int prestigeLevel;
	private PrestigeIslands islandPrestige;
	
	private List<String> players;
	
	private Portal portal;
	private boolean hasPortal;
	Bob bob;
	
	public isOLD(String player){
			this.players = new ArrayList<>();
		
			this.center = IslandManager.getInstance().getLastIslandLocation();
			islandBlocks = PrestigeIslands.INITIAL.getSchem();
			this.islandPrestige = PrestigeIslands.INITIAL;
			
			this.spawnLoc = center;
			
			this.uuid = UUID.randomUUID().toString();
			this.player = player;
			this.prestigeLevel = 0;
			this.hasPortal = false;
			
			String configPath = "Islands." + player;
			
			SettingsManager.getIslands().createSection(configPath);
			SettingsManager.getIslands().set(configPath + ".center", serializeLocation(center, true));
			SettingsManager.getIslands().set(configPath + ".prestigeLevel", prestigeLevel);
			SettingsManager.getIslands().set(configPath + ".uuid", uuid);
			SettingsManager.getIslands().set(configPath + ".builded", false);
			SettingsManager.getIslands().set("Islands." + player + ".hasPortal", hasPortal);
			SettingsManager.getIslands().set("Islands." + player + ".portalLocation", null);
			SettingsManager.getIslands().set("Islands." + player + ".players", Arrays.asList("RichTheLord"));
			getPortalFromConfig();
			
			//SETTING SPAWN LOCATION
			SettingsManager.getIslands().set(configPath + ".spawnLoc", serializeLocation(spawnLoc, true));
			
			if(IslandManager.getInstance().getIslands().contains(IslandManager.getInstance().getIsland(player))){
				IslandManager.getInstance().getIslands().remove(IslandManager.getInstance().getIsland(player));
			}
			
			IslandManager.getInstance().addIsland(this);
			bob = new Bob(center.getWorld());
			try {
				createWorldGuardSection();
				protectBalloon();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	public isOLD(String player, PrestigeIslands prestige){
		this.players = new ArrayList<>();
		this.center = IslandManager.getInstance().getLastIslandLocation();
		islandBlocks = prestige.getSchem();
		this.islandPrestige = prestige;
		
		this.spawnLoc = center;
		
		this.uuid = UUID.randomUUID().toString();
		this.player = player;
		this.prestigeLevel = prestige.getValue();
		this.hasPortal = false;
		
		String configPath = "Islands." + player;
		
		SettingsManager.getIslands().createSection(configPath);
		SettingsManager.getIslands().set(configPath + ".center", serializeLocation(center, true));
		SettingsManager.getIslands().set(configPath + ".prestigeLevel", prestigeLevel);
		SettingsManager.getIslands().set(configPath + ".uuid", uuid);
		SettingsManager.getIslands().set(configPath + ".builded", false);
		SettingsManager.getIslands().set("Islands." + player + ".hasPortal", hasPortal);
		SettingsManager.getIslands().set("Islands." + player + ".portalLocation", null);
		SettingsManager.getIslands().set("Islands." + player + ".players", Arrays.asList("RichTheLord"));
		
		getPortalFromConfig();
		
		//SETTING SPAWN LOCATION
		SettingsManager.getIslands().set(configPath + ".spawnLoc", serializeLocation(spawnLoc, true));
		
		if(IslandManager.getInstance().getIslands().contains(IslandManager.getInstance().getIsland(player))){
			IslandManager.getInstance().getIslands().remove(IslandManager.getInstance().getIsland(player));
		}
		IslandManager.getInstance().addIsland(this);
		bob = new Bob(center.getWorld());
		
		try {
			createWorldGuardSection();
			protectBalloon();
		} catch (Exception e) {
			e.printStackTrace();
		}
}
	
	public isOLD(String player, boolean justconf){
		String path = "Islands." + player;
		
		this.player = player;
		this.center = deserializeLocation(SettingsManager.getIslands().<String>get(path + ".center"));
		this.uuid = SettingsManager.getIslands().<String>get(path + ".uuid");
		this.prestigeLevel = SettingsManager.getIslands().<Integer>get(path + ".prestigeLevel");
		this.spawnLoc = center;//lu.deserializeLocation(SettingsManager.getIslands().<String>get(path + ".spawnLoc"));
		this.islandPrestige = PrestigeIslands.getPrestige(prestigeLevel);
		this.players = new ArrayList<>();
		for(String s : SettingsManager.getIslands().<List<String>>get(path + ".players")){
			players.add(s);
		}
		getPortalFromConfig();
		
		if(IslandManager.getInstance().getIslands().contains(IslandManager.getInstance().getIsland(player))){
			IslandManager.getInstance().getIslands().remove(IslandManager.getInstance().getIsland(player));
		}
		IslandManager.getInstance().getIslands().add(this);
		
		try {
			createWorldGuardSection();
			protectBalloon();
			spawnNPC();
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public List<String> getPlayers(){
		return players;
	}
	
	public void addPlayer(String player){//TODO: FIX THIS
		players.add(player);		
		//Main.getWorldGuard().getRegionManager(Bukkit.getWorld("skyworld")).getRegion("island_" + player).getOwners().addPlayer(player);
		SettingsManager.getIslands().set("Islands." + this.player + ".players", players);
	}
	
	public void removePlayer(String player){
		players.remove(player);
		ProtectedRegion reg = Main.getWorldGuard().getRegionManager(center.getWorld()).getRegion("island_" + player);
		DefaultDomain owners = reg.getOwners();
		owners.removePlayer(player);

		SettingsManager.getIslands().set("Islands." + this.player + ".players", players);
	}
	
	public Location getSpawn(){
		return spawnLoc;
	}
	
	public void setSpawn(Location loc){
		spawnLoc = loc;
	}
	
	public Location getCenter(){
		return center;
	}
	
	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public String getUUID(){ 
		return uuid;
	}
	
	public PrestigeIslands getPrestige(){
		return islandPrestige;
	}
	
	public void setPrestige(PrestigeIslands pi){
		islandPrestige = pi;
	}
	
	public isOLD doPlayerActions(Player player){
		
		player.sendMessage(ChatColor.GREEN + "You are begin teleported to your island!");
		player.teleport(center);
		
		return this;
	}
	
	public void build(){		
		if(SettingsManager.getIslands().<Boolean>get("Islands." + player + ".builded") == true)
			return;
		SettingsManager.getIslands().set("Islands." + player + ".builded", true);		
		SchemUtils.buildSchematic(islandBlocks, center);
		
		//NPC BUILD
		spawnNPC();
	}
	
	public void spawnNPC(){
		for(Entity ent : getNearbyEntities(center, 100)){
			if(ent instanceof Villager){
				EntityVillager znm = ((CraftVillager)ent).getHandle();
				if(znm instanceof Bob){
					return;
				}
			}
		}
		Location npcLoc = center.clone();
		
		if(islandPrestige == PrestigeIslands.INITIAL) {npcLoc.subtract(0, 0, 12.5);npcLoc.add(1.5, 0 ,0);} else if(islandPrestige == PrestigeIslands.PRESTIGE_1){npcLoc.subtract(0, 0, 0);npcLoc.add(10.5, 2,21.5);} else if(islandPrestige == PrestigeIslands.PRESTIGE_2){npcLoc.subtract(8.5, 0, 0);npcLoc.add(0, 2,24.5);}
		else if(islandPrestige == PrestigeIslands.PRESTIGE_3){npcLoc.subtract(28.5, 3, 4.5);npcLoc.add(0, 0, 0);}else if(islandPrestige == PrestigeIslands.PRESTIGE_4){npcLoc.subtract(9.5, 0, 0);npcLoc.add(0, 2, 23.5);}else if(islandPrestige == PrestigeIslands.PRESTIGE_5){npcLoc.subtract(0, 0, 0);npcLoc.add(25.5, 1, 9.5);}
		
		CustomEntities.spawn(bob, npcLoc);
		Block b = npcLoc.clone().subtract(0,1,0).getBlock();
		b.setType(Material.BEDROCK);
	}
	
	public void destroy(){
		this.player = null;
		this.uuid = null;
		
		try {
			portal.destroy();
			portal.breakPortal();
			portal.getTitle().remove();
		} catch (Exception e){
			
		}
		
		for(Entity en : getNearbyEntities(center, 25)){
			if(en instanceof Villager){
				EntityVillager znm = ((CraftVillager)en).getHandle();
				if(znm instanceof Bob){
					en.remove();
				}
			}
		}
		
		IslandManager.getInstance().getIslands().remove(this);
	}
	
	//PORTAL
	public void addPortal(Location l){
		if(hasPortal == false){
			portal = new Portal(this, l);
			portal.build();
			hasPortal = true;
			
			SettingsManager.getIslands().set("Islands." + player + ".hasPortal", hasPortal);
			SettingsManager.getIslands().set("Islands." + player + ".portalLocation", serializeLocation(portal.getBase(), true));
		}
	}
	
	public void getPortalFromConfig(){
		hasPortal = SettingsManager.getIslands().<Boolean>get("Islands." + player + ".hasPortal");
		if(hasPortal == true){
			portal = new Portal(this, deserializeLocation(SettingsManager.getIslands().<String>get("Islands." + player + ".portalLocation")));
		}
	}
	
	public Portal getPortal(){
		if(hasPortal == false){
			return null;
		}
		return portal;
	}
	
	public void saveInConfig(){
		String configPath = "Islands." + player;
		
		SettingsManager.getIslands().createSection(configPath);
		SettingsManager.getIslands().set(configPath + ".center", serializeLocation(center, true));
		SettingsManager.getIslands().set(configPath + ".prestigeLevel", prestigeLevel);
		SettingsManager.getIslands().set(configPath + ".uuid", uuid);
		SettingsManager.getIslands().set(configPath + ".builded", false);
		SettingsManager.getIslands().set(configPath + ".hasPortal", hasPortal);
		SettingsManager.getIslands().set(configPath + ".players", players);
		if(portal != null){
			SettingsManager.getIslands().set(configPath + ".center", serializeLocation(portal.getBase(), true));
		}
		
		//SETTING SPAWN LOCATION
		SettingsManager.getIslands().set(configPath + ".spawnLoc", serializeLocation(spawnLoc, true));
	}
	
	private boolean doesRegionExists(){
		ProtectedRegion reg = Main.getWorldGuard().getRegionManager(center.getWorld()).getRegion("island_" + player);
		if(reg == null){
			return false;
		} else
		return true;
	}
	
	public void createWorldGuardSection() throws Exception{
		
		if(doesRegionExists()){
			Main.getWorldGuard().getRegionManager(center.getWorld()).removeRegion("island_" + player);
		}
		
		Location pointA = center.clone();
		pointA.add(0, 300, 100);
		pointA.subtract(100, 0, 0);
		Location pointB = center.clone();
		pointB.add(100, 0, 00);
		pointB.subtract(0, 97, 100);
		
		ProtectedCuboidRegion reg = new ProtectedCuboidRegion(
				"island_" + player,
				new BlockVector(pointA.getBlockX(), pointA.getBlockY(), pointA.getBlockZ()), 
				new BlockVector(pointB.getBlockX(), pointB.getBlockY(), pointB.getBlockZ())
		);
		DefaultDomain owners = new DefaultDomain();
		owners.addPlayer(player);
		reg.setOwners(owners);
		Main.getWorldGuard().getRegionManager(center.getWorld()).addRegion(reg);
	}
	
	public void protectBalloon() throws Exception{
		if(Main.getWorldGuard().getRegionManager(center.getWorld()).getRegion("island_balloon_" + player) != null){
			Main.getWorldGuard().getRegionManager(center.getWorld()).removeRegion("island_balloon_" + player);
		}
		
		Location pointA = center.clone();
		Location pointB = center.clone();
		if(islandPrestige == PrestigeIslands.INITIAL){
			pointA.add(0, 0, 0);pointA.subtract(6, 0, 8);pointB.add(8, 30, 00);pointB.subtract(0, 2, 19);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_1){
			pointA.add(0, 0, 13);pointA.subtract(19, 5, 0);pointB.add(31, 30, 31);pointB.subtract(0, 0, 0);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_2){
			pointA.add(0, 0, 17);pointA.subtract(39, 5, 0);pointB.add(17, 25 , 36);pointB.subtract(0, 0, 0);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_3){
			pointA.add(0, 0, 0);pointA.subtract(21, 9, 28);pointB.add(0, 19, 21);pointB.subtract(42, 0, 0);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_4){
			pointA.add(0, 0, 15);pointA.subtract(36, 15, 0);pointB.add(18, 34, 37);pointB.subtract(0, 0, 0);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_5){
			pointA.add(16, 0, 25);pointA.subtract(0, 20, 0);pointB.add(36, 21, 00);pointB.subtract(0, 0, 22);
		}
			
		

		
		ProtectedCuboidRegion reg = new ProtectedCuboidRegion(
				"island_balloon_" + player,
				new BlockVector(pointA.getBlockX(), pointA.getBlockY(), pointA.getBlockZ()), 
				new BlockVector(pointB.getBlockX(), pointB.getBlockY(), pointB.getBlockZ())
		);
		Main.getWorldGuard().getRegionManager(center.getWorld()).addRegion(reg);
	}
	
	public String serializeLocation(Location locc, boolean onlyblock){
		String result = null;
		Location loc = locc.clone();
		if(onlyblock){
			result = 
					loc.getBlockX() + ";" +
					loc.getBlockY() + ";" +
					loc.getBlockZ() + ";" +
					loc.getPitch()  + ";" +
					loc.getYaw()    + ";" + 
					loc.getWorld().getName();
					
		} else {
			result = 
					loc.getX() + ";" +
					loc.getY() + ";" +
					loc.getZ() + ";" +
					loc.getPitch()  + ";" +
					loc.getYaw()    + ";" + 
					loc.getWorld().getName();
		}
		
		return result;
	}
	
	public Location deserializeLocation(String object){
		Location res = null;
		String[] data = object.split(";");
		
		res = new Location(Bukkit.getServer().getWorld(data[5]),  // world 
				Double.parseDouble(data[0]), //x
				Double.parseDouble(data[1]), // y
				Double.parseDouble(data[2]), // z
				Float.parseFloat(data[4]), // yaw
				Float.parseFloat(data[3])); // pitch
		return res.clone();
	}

	
	//Thanks heisan123
	public List<Entity> getNearbyEntities(Location where, int range) {
		List<Entity> found = new ArrayList<Entity>();
		 
		for (Entity entity : where.getWorld().getEntities()) {
			if (isInBorder(where, entity.getLocation(), range)) {
				found.add(entity);
			}
		}
		return found;
	}
	
	public boolean isInBorder(Location center, Location notCenter, int range) {
		int x = center.getBlockX(), z = center.getBlockZ();
		int x1 = notCenter.getBlockX(), z1 = notCenter.getBlockZ();
		 
		if (x1 >= (x + range) || z1 >= (z + range) || x1 <= (x - range) || z1 <= (z - range)) {
			return false;
		}
			return true;
		}
}
