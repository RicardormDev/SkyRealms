package com.gitlab.richthelord.SkyRealms.IslandObject;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.gitlab.richthelord.SkyRealms.Main;
import com.gitlab.richthelord.SkyRealms.SettingsManager;

public class Traveler {
	
	private Island island;
	
	private Location location;
	private ArmorStand stand;
	private ArmorStand stand2;
	
	public Traveler(Location location, Island island){
		this.location = location.clone();
		this.island = island;
		
		stand = location.getWorld().spawn(location, ArmorStand.class);
		stand2 = location.getWorld().spawn(location.clone().add(0,0.3,0), ArmorStand.class);
		
		stand.setArms(true);
		stand.setGravity(false);
		
		stand2.setGravity(false);
		stand2.setVisible(false);
		
		ItemStack chestp = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		ItemStack leggins = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		ItemStack boots = new ItemStack(Material.LEATHER_BOOTS, 1);
		
		LeatherArmorMeta chestm = (LeatherArmorMeta) chestp.getItemMeta();
		LeatherArmorMeta legginsm = (LeatherArmorMeta) leggins.getItemMeta();
		LeatherArmorMeta bootsm = (LeatherArmorMeta) boots.getItemMeta();
		
		chestm.setColor(Color.LIME);
		legginsm.setColor(Color.LIME);
		bootsm.setColor(Color.LIME);
		
		chestp.setItemMeta(chestm);
		leggins.setItemMeta(legginsm);
		boots.setItemMeta(bootsm);
		
		ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
		SkullMeta headm = (SkullMeta) head.getItemMeta();
		headm.setOwner(island.getPlayer());
		head.setItemMeta(headm);
		
		stand.setHelmet(head);
		stand.setChestplate(chestp);
		stand.setLeggings(leggins);
		stand.setBoots(boots);
		
		stand.setCustomName("�6" + island.getPlayer() + "'s Island!");
		stand.setCustomNameVisible(true);
		
		stand2.setCustomName("�b" + "Right Click");
		stand2.setCustomNameVisible(true);
	}

	public Location getLocation() {
		return location;
	}

	public ArmorStand[] getStands(){
		return new ArmorStand[] {stand, stand2};
	}
	
	public void destroy() {
		stand.remove();
		stand2.remove();
	}

	public Island getIsland() {
		return island;
	}

	/**
	 * @param island the island to set
	 */
	public void setIsland(Island island) {
		this.island = island;
	}
	
}
