package com.gitlab.richthelord.SkyRealms.IslandObject;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import com.gitlab.richthelord.SkyRealms.SettingsManager;
import com.gitlab.richthelord.SkyRealms.Utils.LocationUtils;

public class IslandManager {
	
	private static IslandManager instance = new IslandManager();
	public static IslandManager getInstance() {
		return instance;
	}
	
	private ArrayList<Island> Islands;
	private static Location lastIslandLocation;
	private static LocationUtils lu;
	public IslandManager(){
		Islands = new ArrayList<>();
		lu = new LocationUtils();
		
		if(!SettingsManager.getIslands().contains("IslandsData")){
			SettingsManager.getIslands().createSection("IslandsData");
			SettingsManager.getIslands().set("IslandsData.LastLocationIsland", lu.serializeLocation(new Location(Bukkit.getWorld("skyworld"), 0, 100, 0, 0, 0), true));		
		}
		lastIslandLocation = lu.deserializeLocation(SettingsManager.getIslands().<String>get("IslandsData.LastLocationIsland"));
	}
	
	
	public Location getLastIslandLocation(){
		return lastIslandLocation;
	}
	
	public Island getIsland(String playerName){
		for(Island Island : Islands){
			if(playerName.equals(Island.getPlayer())){
				return Island;
			}			
		}
		return null;
	}
	
	public void setup(){
		
		if(!SettingsManager.getIslands().contains("Islands"))
			return;		
		
		Islands.clear();
		for(String player : SettingsManager.getIslands().<ConfigurationSection>get("Islands").getKeys(false)){
			new Island(player);
		}
	}
	
	public ArrayList<Island> getIslands(){
		return Islands;
	}
	
	public static void addDistanceLocation(){
		int data = 200 + (int)(Math.random() * 300);
		lastIslandLocation.add(data, 0, data);
		SettingsManager.getIslands().set("IslandsData.LastLocationIsland", lu.serializeLocation(lastIslandLocation, true));
	}
	
	public void addIsland(Island Island){
		Islands.add(Island);
	}

	public void createAnIsland(Player player){
		addDistanceLocation();
		
		SettingsManager.getIslands().set("Islands." + player.getName() + ".center", lu.serializeLocation(lastIslandLocation, false));
		SettingsManager.getIslands().set("Islands." + player.getName() + ".prestigeLevel", 0);
		SettingsManager.getIslands().set("Islands." + player.getName() + ".players", new ArrayList<>());
		SettingsManager.getIslands().set("Islands." + player.getName() + ".builded", false);
		SettingsManager.getIslands().set("Islands." + player.getName() + ".hasTraveler", false);
		
		addDistanceLocation();
	}
	
}
