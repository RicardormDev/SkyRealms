package com.gitlab.richthelord.SkyRealms.IslandObject;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftVillager;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

import com.gitlab.richthelord.SkyRealms.Main;
import com.gitlab.richthelord.SkyRealms.SettingsManager;
import com.gitlab.richthelord.SkyRealms.Utils.LocationUtils;
import com.gitlab.richthelord.SkyRealms.Utils.SchemUtils;
import com.gitlab.richthelord.SkyRealms.bob.NPC.Bob;
import com.gitlab.richthelord.SkyRealms.bob.NPC.CustomEntities;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_9_R1.EntityVillager;

@SuppressWarnings("deprecation")
public class Island {

	private String player;
	
	private CuboidClipboard islandBlocks;
	private Location center;
	
	private int prestigeLevel;
	private PrestigeIslands islandPrestige;
	
	private List<String> players;
	
	private Traveler traveler;
	private boolean hasTraveler;
	private Bob bob;
	
	private LocationUtils lu;
	
	public Island(String player){
		String path = "Islands." + player;
		this.player = player;
		this.lu = new LocationUtils();
		
		this.center = lu.deserializeLocation(SettingsManager.getIslands().<String>get(path + ".center"));
		this.prestigeLevel = SettingsManager.getIslands().<Integer>get(path + ".prestigeLevel");
		this.islandPrestige = PrestigeIslands.getPrestige(prestigeLevel);
		islandBlocks = islandPrestige.getSchem();
		this.players = new ArrayList<>();
		this.hasTraveler = SettingsManager.getIslands().<Boolean>get(path + ".hasTraveler");
		for(String s : SettingsManager.getIslands().<List<String>>get(path + ".players")){
			players.add(s);
		}
		
		if(hasTraveler == true){
			traveler = new Traveler(lu.deserializeLocation(SettingsManager.getIslands().<String>get(path + ".travelerLocation")), this);
		}
		
		//BOB SECTION
		bob = new Bob(center.getWorld());
		
		//WORLDGUARD CONTRUCTOR
		try {
			createWorldGuardSection();
			protectBalloon();
		} catch (Exception e) {
			Main.log.log(Level.SEVERE, "[SkyRealms-BOT] I found a bug on isOLD Contructor inside worldguard section");
		}
		
		
		IslandManager.getInstance().getIslands().add(this);
	}
	
	public String getPlayer(){
		return player;
	}
	
	public Location getCenter(){
		return center;
	}
	
	public PrestigeIslands getPrestige(){
		return islandPrestige;
	}
	
	public List<String> getInvitedPlayers(){
		return players;
	}

	public Traveler geTraveler(){
		return traveler;
	}
	
	public boolean hasTraveler(){
		return hasTraveler;
	}
	
	public Bob getBob(){
		return bob;
	}
	
	/*
	 * 
	 * === BUILD SECTION ===
	 * 
	 **/
	
	public void build(){		
		if(SettingsManager.getIslands().<Boolean>get("Islands." + player + ".builded") == true)
			return;
		SettingsManager.getIslands().set("Islands." + player + ".builded", true);		
		SchemUtils.buildSchematic(islandBlocks, center);
		
		//NPC BUILD
		spawnNPC(false);
	}
	
	public Island doPlayerActions(Player player){
		
		player.sendMessage(ChatColor.GREEN + "Commening teleportation to your island");
		player.teleport(center);
		
		return this;
	}
	
	/*
	 * 
	 * === BOB SECTION ===
	 * 
	 **/
	
	public void spawnNPC(boolean force){
		if(force == false){
			for(Entity ent : lu.getNearbyEntities(center, 100)){
				if(ent instanceof Villager){
					EntityVillager znm = ((CraftVillager)ent).getHandle();
					if(znm instanceof Bob){
						return;
					}
				}
			}
		}
		Location npcLoc = center.clone();
		
		if(islandPrestige == PrestigeIslands.INITIAL) {npcLoc.subtract(0, 0, 12.5);npcLoc.add(1.5, 0 ,0);} else if(islandPrestige == PrestigeIslands.PRESTIGE_1){npcLoc.subtract(0, 0, 0);npcLoc.add(10.5, 2,21.5);} else if(islandPrestige == PrestigeIslands.PRESTIGE_2){npcLoc.subtract(8.5, 0, 0);npcLoc.add(0, 2,24.5);}
		else if(islandPrestige == PrestigeIslands.PRESTIGE_3){npcLoc.subtract(28.5, 3, 4.5);npcLoc.add(0, 0, 0);}else if(islandPrestige == PrestigeIslands.PRESTIGE_4){npcLoc.subtract(9.5, 0, 0);npcLoc.add(0, 2, 23.5);}else if(islandPrestige == PrestigeIslands.PRESTIGE_5){npcLoc.subtract(0, 0, 0);npcLoc.add(25.5, 1, 9.5);}
		
		Bob bb = bob;
		
		if(force){
			bb = new Bob(center.getWorld());
		}
		
		CustomEntities.spawn(bb, npcLoc);
		Block b = npcLoc.clone().subtract(0,1,0).getBlock();
		b.setType(Material.BEDROCK);
	}
	
	/*
	 * 
	 * === PLAYERS SECTION ===
	 * 
	 **/
	
	public void addPlayer(String player){
		players.add(player);		
		Main.getWorldGuard().
		getRegionManager(Bukkit.getWorld("skyworld")).
		getRegion("island_" + this.player).
		getOwners().
		addPlayer(player);
		
		SettingsManager.getIslands().set("Islands." + this.player + ".players", players);
	}
	
	public void removePlayer(String player){
		players.remove(player);
		Main.getWorldGuard().getRegionManager(Bukkit.getWorld("skyworld")).getRegion("island_" + this.player).getOwners().removePlayer(player);
		SettingsManager.getIslands().set("Islands." + this.player + ".players", players);
	}
	
	/*
	 * 
	 * === WORLD GUARD SECTION ===
	 * 
	 **/
	
	private boolean doesRegionExists(){
		ProtectedRegion reg = Main.getWorldGuard().getRegionManager(center.getWorld()).getRegion("island_" + player);
		if(reg == null){
			return false;
		} else
		return true;
	}
	
	public void createWorldGuardSection() throws Exception{
		
		if(doesRegionExists()){
			Main.getWorldGuard().getRegionManager(center.getWorld()).removeRegion("island_" + player);
		}
		
		Location pointA = center.clone();
		pointA.add(0, 300, 100);
		pointA.subtract(100, 0, 0);
		Location pointB = center.clone();
		pointB.add(100, 0, 00);
		pointB.subtract(0, 97, 100);
		
		ProtectedCuboidRegion reg = new ProtectedCuboidRegion(
				"island_" + player,
				new BlockVector(pointA.getBlockX(), pointA.getBlockY(), pointA.getBlockZ()), 
				new BlockVector(pointB.getBlockX(), pointB.getBlockY(), pointB.getBlockZ())
		);
		DefaultDomain owners = new DefaultDomain();
		owners.addPlayer(player);
		reg.setOwners(owners);
		Main.getWorldGuard().getRegionManager(center.getWorld()).addRegion(reg);
	}
	
	public void protectBalloon() throws Exception{
		if(Main.getWorldGuard().getRegionManager(center.getWorld()).getRegion("island_balloon_" + player) != null){
			Main.getWorldGuard().getRegionManager(center.getWorld()).removeRegion("island_balloon_" + player);
		}
		
		Location pointA = center.clone();
		Location pointB = center.clone();
		if(islandPrestige == PrestigeIslands.INITIAL){
			pointA.add(0, 0, 0);pointA.subtract(6, 0, 8);pointB.add(8, 30, 00);pointB.subtract(0, 2, 19);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_1){
			pointA.add(0, 0, 13);pointA.subtract(19, 5, 0);pointB.add(31, 30, 31);pointB.subtract(0, 0, 0);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_2){
			pointA.add(0, 0, 17);pointA.subtract(39, 5, 0);pointB.add(17, 25 , 36);pointB.subtract(0, 0, 0);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_3){
			pointA.add(0, 0, 0);pointA.subtract(21, 9, 28);pointB.add(0, 19, 21);pointB.subtract(42, 0, 0);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_4){
			pointA.add(0, 0, 15);pointA.subtract(36, 15, 0);pointB.add(18, 34, 37);pointB.subtract(0, 0, 0);
		} else if(islandPrestige == PrestigeIslands.PRESTIGE_5){
			pointA.add(16, 0, 25);pointA.subtract(0, 20, 0);pointB.add(36, 21, 00);pointB.subtract(0, 0, 22);
		}
			
		

		
		ProtectedCuboidRegion reg = new ProtectedCuboidRegion(
				"island_balloon_" + player,
				new BlockVector(pointA.getBlockX(), pointA.getBlockY(), pointA.getBlockZ()), 
				new BlockVector(pointB.getBlockX(), pointB.getBlockY(), pointB.getBlockZ())
		);
		Main.getWorldGuard().getRegionManager(center.getWorld()).addRegion(reg);
	}
	
	/*
	 * 
	 * === SAVING SECTION ===
	 * 
	 **/
	
	public void saveInConfig(){
		String configPath = "Islands." + player;
		
		SettingsManager.getIslands().createSection(configPath);
		SettingsManager.getIslands().set(configPath + ".center", lu.serializeLocation(center, true));
		SettingsManager.getIslands().set(configPath + ".prestigeLevel", prestigeLevel);
		SettingsManager.getIslands().set(configPath + ".builded", false);
		SettingsManager.getIslands().set(configPath + ".hasTraveler", hasTraveler);
		SettingsManager.getIslands().set(configPath + ".players", players);
		if(traveler != null){
			SettingsManager.getIslands().set(configPath + ".travelerLocation", lu.serializeLocation(traveler.getLocation(), true));
		}
	}
	
	/*
	 * 
	 * === TRAVELER SECTION ===
	 * 
	 **/
	
	public void createPortal(Location l){
		if(hasTraveler == false){
			traveler = new Traveler(l, this);
			hasTraveler = true;
			
			SettingsManager.getIslands().set("Islands." + player + ".hasTraveler", hasTraveler);
			SettingsManager.getIslands().set("Islands." + player + ".travelerLocation", lu.serializeLocation(traveler.getLocation(), false));
		}
	}
	
	/*
	 * 
	 * === DESTROY SECTION ===
	 * 
	 **/
	
	public Island resetWithPrestige(PrestigeIslands prestige){
		String configPath = "Islands." + player;
		
		IslandManager.addDistanceLocation();
		center = IslandManager.getInstance().getLastIslandLocation();center = IslandManager.getInstance().getLastIslandLocation();
		
		SettingsManager.getIslands().createSection(configPath);
		SettingsManager.getIslands().set(configPath + ".center", lu.serializeLocation(center, true));
		SettingsManager.getIslands().set(configPath + ".prestigeLevel", prestige.getValue());
		SettingsManager.getIslands().set(configPath + ".builded", false);
		SettingsManager.getIslands().set(configPath + ".hasTraveler", hasTraveler);
		SettingsManager.getIslands().set(configPath + ".players", players);
		if(traveler != null){
			SettingsManager.getIslands().set(configPath + ".travelerLocation", lu.serializeLocation(traveler.getLocation(), true));
		}
		islandBlocks = prestige.getSchem();
		prestigeLevel = prestige.getValue();
		islandPrestige = prestige;
				
		bob.getBukkitEntity().remove();
		bob = new Bob(center.getWorld());
		SchemUtils.buildSchematic(islandBlocks, center);
		SettingsManager.getIslands().set(configPath + ".builded", true);
		spawnNPC(true);
		
		try {
			createWorldGuardSection();
			protectBalloon();
		} catch (Exception e) {
			Main.log.log(Level.SEVERE, "[SkyRealms-BOT] I found a bug on isOLD Contructor inside worldguard section");
		}
		
		saveInConfig();
		return this;
	}
	
	public void fixNPC(){
		for(Entity ent : lu.getNearbyEntities(center, 150)){
			if(ent instanceof Villager){
				ent.remove();
			}
		}
		spawnNPC(true);
	}
	
	public void destroy(){
		this.player = null;
		
		try {
			traveler.destroy();
		} catch (Exception e){ }
		
		for(Entity en : lu.getNearbyEntities(center, 25)){
			if(en instanceof Villager){
				EntityVillager znm = ((CraftVillager)en).getHandle();
				if(znm instanceof Bob){
					en.remove();
				}
			}
		}
		
		SettingsManager.getIslands().set("Islands." + player, null);
		IslandManager.getInstance().getIslands().remove(this);
	}
	
}
