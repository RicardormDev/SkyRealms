package com.gitlab.richthelord.SkyRealms.IslandObject;

import com.gitlab.richthelord.SkyRealms.Utils.SchemUtils;
import com.sk89q.worldedit.CuboidClipboard;

@SuppressWarnings("deprecation")
public enum PrestigeIslands{
	INITIAL(SchemUtils.getSchematicIsland("first-island"), 0, 0, "[B]"),
	PRESTIGE_1(SchemUtils.getSchematicIsland("prestige-1"), 10, 10000, "[➀✰]"),
	PRESTIGE_2(SchemUtils.getSchematicIsland("prestige-2"), 20, 20000, "[➁✰]"),
	PRESTIGE_3(SchemUtils.getSchematicIsland("prestige-3"), 30, 30000, "[➂✰]"),
	PRESTIGE_4(SchemUtils.getSchematicIsland("prestige-4"), 40, 40000, "[➃✰]"),
	PRESTIGE_5(SchemUtils.getSchematicIsland("prestige-5"), 50, 50000, "[➄✰]");
	
	private final CuboidClipboard schem;
	private final int value;
	private final int cost;
	private final String chatFormat;
	
	private PrestigeIslands(CuboidClipboard schem, int value, int cost, String chatFormat){
		this.schem = schem;
		this.value = value;
		this.cost = cost;
		this.chatFormat = chatFormat;
	}
	
	public int getCost(){
		return cost;
	}
	
	public CuboidClipboard getSchem(){
		return schem;
	}
	
	public int getValue(){
		return value;
	}
	
	public static PrestigeIslands getMaxPrestige(int in){
		PrestigeIslands p = null;
		for(PrestigeIslands pi : PrestigeIslands.values()){			
			if(p == null) p = pi;				
			if(p.getValue() > pi.getValue() && pi.getValue() < in){
				p = pi;
			}
		}
		return p;
	}
	
	public static PrestigeIslands getNext(PrestigeIslands pi){		
		int now = pi.getValue() + 10;
		
		if(now >= 50){
			return PrestigeIslands.PRESTIGE_5;
		}
		
		for(PrestigeIslands pp : PrestigeIslands.values()){
			if(pp.getValue() == now){
				return pp;
			}
		}
		
		return null;
	}
	
	public static PrestigeIslands getPrestige(int i){
		for(PrestigeIslands pi : PrestigeIslands.values()){
			if(pi.getValue() == i){
				return pi;
			}
		}
		return null;
	}

	public CharSequence getChatFormat() {
		return chatFormat;
	}
	
}
