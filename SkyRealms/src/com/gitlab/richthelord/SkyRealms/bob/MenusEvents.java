package com.gitlab.richthelord.SkyRealms.bob;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import com.gitlab.richthelord.SkyRealms.ConfirmReset;
import com.gitlab.richthelord.SkyRealms.Main;
import com.gitlab.richthelord.SkyRealms.IslandObject.Island;
import com.gitlab.richthelord.SkyRealms.IslandObject.IslandManager;
import com.gitlab.richthelord.SkyRealms.IslandObject.PrestigeIslands;
import com.gitlab.richthelord.SkyRealms.Utils.MoneyUtils;
import com.gitlab.richthelord.SkyRealms.bob.Menus.BobPrestige;

import net.md_5.bungee.api.ChatColor;

public class MenusEvents implements Listener{
	
	@EventHandler
	public void bob(InventoryClickEvent e){
		if(!e.getInventory().getName().contains("Bob | Home"))
			return;
		final Player player = (Player) e.getWhoClicked();
		e.setCancelled(true);
		
		if(e.getCurrentItem().getType() == Material.AIR || e.getCurrentItem() == null || e.getCurrentItem().hasItemMeta() == false){
			return;
		}
		
		switch(e.getCurrentItem().getType()){
		case BEDROCK:
			player.closeInventory();
			new ConfirmReset(player) {
				
				@Override
				public void action() {
					player.sendMessage(ChatColor.GREEN + "Reseting your island");
					Island c = IslandManager.getInstance().getIsland(player.getName());
					c.resetWithPrestige(c.getPrestige()).doPlayerActions(player);
				}
			}.open();
			break;
		case BEACON:
			player.closeInventory();
			new BobPrestige(player).build(player);
			break;
		case EMERALD:
			Main.shop.openShop(player);
			break;
		case AIR:
			break;
			default:
				break;
		}
		
	}
	
	@EventHandler
	public void bobPrestige(InventoryClickEvent e){
		if(!e.getInventory().getName().contains("Bob | Prestige"))
			return;
		Player player = (Player) e.getWhoClicked();
		e.setCancelled(true);
		
		switch(e.getCurrentItem().getType()){
		case NETHER_STAR:
			Island is = IslandManager.getInstance().getIsland(player.getName());	
			int cost = PrestigeIslands.getNext(is.getPrestige()).getCost();
			if(MoneyUtils.getBalance(player) < cost){
				player.sendMessage(ChatColor.RED + "You can't afford this");
				return;
			}
			MoneyUtils.removeMoney(player, cost);
			player.sendMessage(ChatColor.GREEN + "$" + cost + " deducted from your account!");
			
			is.resetWithPrestige(PrestigeIslands.getNext(is.getPrestige())).doPlayerActions(player);
			break;
		case BEDROCK:
			player.closeInventory();
			player.sendMessage(ChatColor.RED + "You can't upgrade more your island!");
			break;
		case AIR:
			break;
			default:
				break;
		}
		
	}
	
}
