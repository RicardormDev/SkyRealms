package com.gitlab.richthelord.SkyRealms.bob.Menus;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gitlab.richthelord.SkyRealms.bob.BobMenu;
import com.gitlab.richthelord.SkyRealms.bob.Item;

import net.md_5.bungee.api.ChatColor;

public class BobPrincipal extends BobMenu{
	
	public BobPrincipal(){
		super(3, "Home");
		
		ItemStack upgrade = new ItemStack(Material.BEACON);
		ItemStack shop = new ItemStack(Material.EMERALD);
		ItemStack reset = new ItemStack(Material.BEDROCK);
		
		ItemMeta um = upgrade.getItemMeta();
		ItemMeta sm = shop.getItemMeta();
		ItemMeta rm = reset.getItemMeta();
		
		um.setDisplayName(ChatColor.GOLD + "Upgrade (prestige islands)");
		sm.setDisplayName(ChatColor.GREEN + "Shop");
		rm.setDisplayName(ChatColor.RED + "Reset Your Island");
		
		upgrade.setItemMeta(um);
		shop.setItemMeta(sm);
		reset.setItemMeta(rm);
		
		this.setItems(
				Arrays.asList(
				new Item(upgrade, 10),
				new Item(shop, 13),
				new Item(reset, 16))
				);
	}
	
}
