package com.gitlab.richthelord.SkyRealms.bob.Menus;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gitlab.richthelord.SkyRealms.IslandObject.IslandManager;
import com.gitlab.richthelord.SkyRealms.IslandObject.PrestigeIslands;
import com.gitlab.richthelord.SkyRealms.IslandObject.Island;
import com.gitlab.richthelord.SkyRealms.bob.BobMenu;
import com.gitlab.richthelord.SkyRealms.bob.Item;

import net.md_5.bungee.api.ChatColor;

public class BobPrestige extends BobMenu{
	
	public BobPrestige(Player player){
		super(3, "Prestige");
		
		String toUpgrade = "";
		String cost = "";
		
		Island Island = IslandManager.getInstance().getIsland(player.getName());
		PrestigeIslands next = PrestigeIslands.getNext(Island.getPrestige());
		
		toUpgrade = String.valueOf(next).toLowerCase().replace("_", " ");
		cost = String.valueOf(next.getCost());
		
		ItemStack upgrade = new ItemStack(Material.NETHER_STAR);
		if(Island.getPrestige() == PrestigeIslands.PRESTIGE_5){
			upgrade = new ItemStack(Material.BEDROCK);
			toUpgrade = "You can no longer upgrade your island";
			cost = "TYou can no longer upgrade your island";
		}
				
		ItemMeta um = upgrade.getItemMeta();
		um.setDisplayName(ChatColor.GOLD + "Upgrade to: " + ChatColor.GREEN + toUpgrade);
		um.setLore(Arrays.asList(ChatColor.AQUA + "This costs: " + cost, "",
				ChatColor.GRAY + "Your current island: " + String.valueOf(Island.getPrestige()).toLowerCase().replace("_", " ")));
		upgrade.setItemMeta(um);
		
		this.setItems(Arrays.asList(new Item(upgrade, 13)));
	}
	
}
