package com.gitlab.richthelord.SkyRealms.bob;

import org.bukkit.inventory.ItemStack;

public class Item {
	
	private final ItemStack item;
	private final int slot;
	
	public Item(ItemStack item, int slot){
		this.item = item;
		this.slot = slot;
	}
	
	public ItemStack getItem(){
		return item;
	}
	
	public int getSlot(){
		return slot;
	}
	
}
