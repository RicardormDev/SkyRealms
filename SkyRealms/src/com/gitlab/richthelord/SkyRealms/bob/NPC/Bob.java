package com.gitlab.richthelord.SkyRealms.bob.NPC;

import java.lang.reflect.Field;

import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_9_R1.CraftWorld;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;

import net.minecraft.server.v1_9_R1.DamageSource;
import net.minecraft.server.v1_9_R1.EntityHuman;
import net.minecraft.server.v1_9_R1.EntityVillager;
import net.minecraft.server.v1_9_R1.PathfinderGoalLookAtPlayer;

public class Bob extends EntityVillager {
    public Bob(org.bukkit.World world) {
        super(((CraftWorld)world).getHandle());
     //   List goalB = (List)getPrivateField("b", PathfinderGoalSelector.class, goalSelector); goalB.clear();
     //   List goalC = (List)getPrivateField("c", PathfinderGoalSelector.class, goalSelector); goalC.clear();
     //   List targetB = (List)getPrivateField("b", PathfinderGoalSelector.class, targetSelector); targetB.clear();
     //   List targetC = (List)getPrivateField("c", PathfinderGoalSelector.class, targetSelector); targetC.clear();

        this.goalSelector.a(8, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 100.0F));
        
        this.setCustomName(ChatColor.GOLD + "Bob");
        this.setCustomNameVisible(true);
        this.setProfession(1);
        ((Villager)this.getBukkitEntity()).setRemoveWhenFarAway(false);;
    }
    
    @SuppressWarnings({ "deprecation" })
	public Bob(net.minecraft.server.v1_9_R1.World world) {
        super(world);
      //  List goalB = (List)getPrivateField("b", PathfinderGoalSelector.class, goalSelector); goalB.clear();
      //  List goalC = (List)getPrivateField("c", PathfinderGoalSelector.class, goalSelector); goalC.clear();
       // List targetB = (List)getPrivateField("b", PathfinderGoalSelector.class, targetSelector); targetB.clear();
       // List targetC = (List)getPrivateField("c", PathfinderGoalSelector.class, targetSelector); targetC.clear();

        this.goalSelector.a(8, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 100.0F));
        
        this.setCustomName(ChatColor.GOLD + "Bob");
        this.setCustomNameVisible(true);
        this.setProfession(Profession.LIBRARIAN.getId());
        ((Villager)this.getBukkitEntity()).setRemoveWhenFarAway(false);;
    }
    
    @Override
    public void move(double d0, double d1, double d2){
        
    }
    
    @Override
    public boolean damageEntity(DamageSource damagesource, float f){
    	return false;
    }
    
    @Override
    public void g(double d0, double d1, double d2) {
    }
    
    @SuppressWarnings("rawtypes")
	public static Object getPrivateField(String fieldName, Class cass, Object object)
    {
        Field field;
        Object o = null;
        try
        {
            field = cass.getDeclaredField(fieldName);
            field.setAccessible(true);
            o = field.get(object);
        }
        catch(NoSuchFieldException e)
        {
            e.printStackTrace();
        }
        catch(IllegalAccessException e)
        {
            e.printStackTrace();
        }
        return o;
    }
}