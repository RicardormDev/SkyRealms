package com.gitlab.richthelord.SkyRealms.bob.NPC;

import java.lang.reflect.Field;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_9_R1.CraftWorld;

import net.minecraft.server.v1_9_R1.Entity;

public enum CustomEntities {
	BOB("Villager", 120, Bob.class);
	
	private CustomEntities(String name, int id, Class<? extends Entity> custom){
		addToMaps(custom, name, id);
	}
	
	public static void spawn(Entity entity, Location loc){
		 entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		((CraftWorld)loc.getWorld()).getHandle().addEntity(entity);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes"})
	private static void addToMaps(Class clazz, String name, int id){	
        ((Map)getPrivateField("c", net.minecraft.server.v1_9_R1.EntityTypes.class, null)).put(name, clazz);
        ((Map)getPrivateField("d", net.minecraft.server.v1_9_R1.EntityTypes.class, null)).put(clazz, name);
        ((Map)getPrivateField("f", net.minecraft.server.v1_9_R1.EntityTypes.class, null)).put(clazz, Integer.valueOf(id));
	}
	
	public static void addToMapsAll(){
		addToMaps(Bob.class, "Villager", 120);
	}
	
	public static Object getPrivateField(String fieldName, Class<?> c, Object object){
		Field field;
		Object o = null;
		
		try {
			field = c.getDeclaredField(fieldName);
			field.setAccessible(true);
			o = field.get(object);
		} catch(NoSuchFieldException e){
			e.printStackTrace();
		} catch(IllegalAccessException e){
			e.printStackTrace();
		}
		return o;
	}
}	
