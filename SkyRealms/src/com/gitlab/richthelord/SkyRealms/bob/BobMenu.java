package com.gitlab.richthelord.SkyRealms.bob;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public abstract class BobMenu{

	private Inventory inv;
	private int size;
	
	private List<Item> items;
	
	public BobMenu(int inventorySize, String title){
		this.size = inventorySize;
		this.inv = Bukkit.getServer().createInventory(null, (9*size), "Bob | " + title);
		
		items = new ArrayList<>();
	}
	
	public void setItems(List<Item> items){
		this.items = items;
		for(Item item : this.items){
			inv.setItem(item.getSlot(), item.getItem());
		}
	}
	
	public List<Item> getItems(){
		return items;
	}
	
	public Item getItemFromSlot(int slot){
		for(Item item : items){
			if(item.getSlot() == slot){
				return item;
			}
		}
		return null;
	}
	
	public void build(Player player){
		player.openInventory(inv);
	}
	
	
	
}
