package com.gitlab.richthelord.SkyRealms.Shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gitlab.richthelord.SkyRealms.Main;
import com.gitlab.richthelord.SkyRealms.SettingsManager;
import com.gitlab.richthelord.SkyRealms.Utils.MoneyUtils;

import net.md_5.bungee.api.ChatColor;

public class ShopListener implements Listener{
	static Main main;	
	FileConfiguration shop;
	public ShopListener(Main i){
		main = i;
		shop = SettingsManager.getShop().getRawFile();
	}
	
	HashMap<Player, String> activeMenu = new HashMap<>();
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void clickInventorySections(InventoryClickEvent e){
		if(!e.getInventory().getName().contains(shop.getString("Shop.Title").replaceAll("&", "�")))
			return;
		e.setCancelled(true);
		ItemStack item = e.getCurrentItem();
		
		if(item.getType() == Material.AIR || item == null)
			return;		
		
		if(item.hasItemMeta()){
			ArrayList<Item> items = new ArrayList<>();
			for(String key : shop.getConfigurationSection("Shop.sections").getKeys(false)){
				String path = "Shop.sections." + key;
				if(item.getItemMeta().getDisplayName().contains(ChatColor.translateAlternateColorCodes("&".charAt(0), shop.getString(path + ".DisplayName")))){					
					for(String it : shop.getConfigurationSection(path + ".ItemsIn").getKeys(false)){
						String pathItem = path + ".ItemsIn." + it;
						
						String[] iconData = shop.getString(pathItem + ".Item").split(":");
						int iconItem = Integer.parseInt(iconData[0]);
						byte iconType = 0; //Byte.parseByte(iconData[1]);
						
						List<String> lore = new ArrayList<>();
						for(String str : shop.getStringList(pathItem + ".Lore")){
							lore.add(ChatColor.translateAlternateColorCodes("&".charAt(0), str.replace("{cost}", ""+shop.getInt(pathItem + ".Price"))));
						}
						
						ItemStack tt = new ItemStack(iconItem, 1, (short)1, iconType);
						ItemMeta meta = tt.getItemMeta();
						meta.setDisplayName(shop.getString(pathItem + ".DisplayName").replaceAll("&", "�").replace("{cost}", ""+shop.getInt(pathItem + ".Price")));
						
						if(!lore.isEmpty()){
							meta.setLore(lore);
						}
						
						tt.setItemMeta(meta);
						
						items.add(new Item(tt, shop.getInt(pathItem + ".Slot")));
					}					
					Shop shopGui = new Shop(shop.getString(path + ".DisplayName"), 
							shop.getInt(path + ".Size"), 
							items);
					e.getWhoClicked().closeInventory();
					e.getWhoClicked().openInventory(shopGui.getInventory());
					activeMenu.put((Player)e.getWhoClicked(), shop.getString(path + ".DisplayName"));
				}
			}			
		}
	}
	
	@EventHandler
	public void clickInventoryShops(InventoryClickEvent e){
		Player player = (Player) e.getWhoClicked();
		if(!activeMenu.containsKey(player))
			return;	
		
		if(!e.getInventory().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes("&".charAt(0), activeMenu.get(player))))
			return;
		e.setCancelled(true);		
		ItemStack item = e.getCurrentItem();
		
		if(item == null || item.getType() == Material.AIR)
			return;
		
		for(String key : shop.getConfigurationSection("Shop.sections").getKeys(false)){
			String path = "Shop.sections." + key;
			if(shop.getString(path + ".DisplayName").contains(activeMenu.get(player))){				
				for(String it : shop.getConfigurationSection(path + ".ItemsIn").getKeys(false)){
					
					if(item.getItemMeta().getDisplayName().contains(ChatColor.translateAlternateColorCodes("&".charAt(0), shop.getString(path + ".ItemsIn." + it + ".DisplayName")))){
						String pathy = path + ".ItemsIn." + it;

						
						if(!activeMenu.get(player).contains("&b&lSell Shop")){
							if(MoneyUtils.getBalance(player) < shop.getInt(pathy + ".Price")){
								
								int need = shop.getInt(pathy + ".Price") - (int)MoneyUtils.getBalance(player);
								int have = (int)MoneyUtils.getBalance(player);
								
								player.sendMessage(new String[]{ChatColor.RED + "You can't afford this!", ChatColor.RED + "You have: " + have, ChatColor.RED + "You need: " + need});
								return;
							}
							
							player.sendMessage(ChatColor.RED + "$" + shop.getInt(pathy + ".Price") + " deducted from your account!");
							MoneyUtils.removeMoney(player, (shop.getDouble(pathy + ".Price")));
						}
						
						
						player.closeInventory();
						//player.getInventory().addItem(new ItemStack(item.getType()));
						
						for(String cmd : shop.getStringList(pathy + ".Commands")){
							Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), cmd.replace("{player}", e.getWhoClicked().getName()));
						}						
					}
				}					
			}
		}
		
	}
	
	@EventHandler
	public void closeInvShop(InventoryCloseEvent e){
		Player player = (Player) e.getPlayer();
		if(activeMenu.containsKey(player)){
			activeMenu.remove(player);
		}
	}
}
