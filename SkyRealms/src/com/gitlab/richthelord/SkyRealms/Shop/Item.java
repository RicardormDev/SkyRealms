package com.gitlab.richthelord.SkyRealms.Shop;

import org.bukkit.inventory.ItemStack;

public class Item {

	private int slot;
	private ItemStack item;
	
	public Item(ItemStack item, int slot){
		this.slot = slot;
		this.item = item;
	}
	
	public ItemStack getItem(){
		return item;
	}
	
	public int getSlot(){
		return slot;
	}
	
}
