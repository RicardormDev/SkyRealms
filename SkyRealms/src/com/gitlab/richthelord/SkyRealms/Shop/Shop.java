package com.gitlab.richthelord.SkyRealms.Shop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

public class Shop {
	
	private Inventory inv;
	private List<Item> items;
	
	public Shop(String name, int size, ArrayList<Item> items){
		inv = Bukkit.createInventory(null, 9 * size, name.replaceAll("&", "�"));
		this.items = items;
		
		for(Item item : items){
			inv.setItem(item.getSlot(), item.getItem());
		}
	}
	
	public Shop(String name, int size, List<Item> items){
		inv = Bukkit.createInventory(null, 9 * size, name.replaceAll("&", "�"));
		this.items = items;
		
		for(Item item : items){
			inv.setItem(item.getSlot(), item.getItem());
		}
	}
	
	public Inventory getInventory(){
		return inv;
	}
	
	public List<Item> getItems(){
		return items;
	}
}