package com.gitlab.richthelord.SkyRealms.Shop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gitlab.richthelord.SkyRealms.SettingsManager;

import net.md_5.bungee.api.ChatColor;

public class Menu{
	
	@SuppressWarnings("deprecation")
	public void openShop(Player player){
		FileConfiguration shop = SettingsManager.getShop().getRawFile();
		
		Inventory inv = Bukkit.createInventory(null, (9 * shop.getInt("Shop.Size")),
				shop.getString("Shop.Title").replaceAll("&", "�"));

		for(String key : shop.getConfigurationSection("Shop.sections").getKeys(false)){
			String path = "Shop.sections." + key;
			
			String[] iconData = shop.getString(path + ".icon").split(":");
			int iconItem = Integer.parseInt(iconData[0]);
			byte iconType = Byte.parseByte(iconData[1]);
			
			List<String> lore = new ArrayList<>();
			for(String str : shop.getStringList(path + ".Lore")){
				lore.add(ChatColor.translateAlternateColorCodes("&".charAt(0), str));
			}
			
			ItemStack item = new ItemStack(iconItem, 1, (short)1, iconType);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(shop.getString(path + ".DisplayName").replaceAll("&", "�"));
			
			if(!lore.isEmpty()){
				meta.setLore(lore);
			}
			
			item.setItemMeta(meta);
			
			inv.setItem(shop.getInt(path + ".Slot"), item);			
		}
		
		player.openInventory(inv);
	}	
}