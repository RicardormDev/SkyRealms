package com.gitlab.richthelord.SkyRealms.Utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;

public class LocationUtils {
	
	public String serializeLocation(Location locc, boolean onlyblock){
		String result = null;
		Location loc = locc.clone();
		if(onlyblock){
			result = 
					loc.getBlockX() + ";" +
					loc.getBlockY() + ";" +
					loc.getBlockZ() + ";" +
					loc.getPitch()  + ";" +
					loc.getYaw()    + ";" + 
					loc.getWorld().getName();
					
		} else {
			result = 
					loc.getX() + ";" +
					loc.getY() + ";" +
					loc.getZ() + ";" +
					loc.getPitch()  + ";" +
					loc.getYaw()    + ";" + 
					loc.getWorld().getName();
		}
		
		return result;
	}
	
	public Location deserializeLocation(String object){
		Location res = null;
		String[] data = object.split(";");
		
		res = new Location(Bukkit.getServer().getWorld(data[5]),  // world 
				Double.parseDouble(data[0]), //x
				Double.parseDouble(data[1]), // y
				Double.parseDouble(data[2]), // z
				Float.parseFloat(data[4]), // yaw
				Float.parseFloat(data[3])); // pitch
		return res.clone();
	}
	
	//Thanks heisan123
	public List<Entity> getNearbyEntities(Location where, int range) {
		List<Entity> found = new ArrayList<Entity>();
		 
		for (Entity entity : where.getWorld().getEntities()) {
			if (isInBorder(where, entity.getLocation(), range)) {
				found.add(entity);
			}
		}
		return found;
	}
	
	public boolean isInBorder(Location center, Location notCenter, int range) {
		int x = center.getBlockX(), z = center.getBlockZ();
		int x1 = notCenter.getBlockX(), z1 = notCenter.getBlockZ();
		 
		if (x1 >= (x + range) || z1 >= (z + range) || x1 <= (x - range) || z1 <= (z - range)) {
			return false;
		}
			return true;
		}
	
}
