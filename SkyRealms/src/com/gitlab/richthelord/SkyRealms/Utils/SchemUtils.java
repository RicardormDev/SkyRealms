package com.gitlab.richthelord.SkyRealms.Utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.Location;
import org.bukkit.World;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;

@SuppressWarnings("deprecation")
public class SchemUtils {
	public static void loadSchem(String schem, Location loc){
		File file = new File("plugins/SkyRealms/schematics/" + schem + ".schematic");
		try{
			Vector v = new Vector(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
			BukkitWorld bw = new BukkitWorld(loc.getWorld());
			//EditSession es = new EditSession(bw, Integer.MAX_VALUE);
			EditSession es = WorldEdit.getInstance().getEditSessionFactory().getEditSession(bw, 0x2b9ac9ff);
			CuboidClipboard cl = SchematicFormat.MCEDIT.load(file);
			cl.place(es, v, false);
			
		} catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public static boolean loadSchemB(String schem, World world, Location loc) throws MaxChangedBlocksException, IOException, DataException{
		File file = new File("plugins/SkyRealms/schematics/" + schem + ".schematic");
		Vector v = new Vector(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
		SchematicFormat format = SchematicFormat.getFormat(file);
		if(format == null)
			return false;
		EditSession es = WorldEdit.getInstance().getEditSessionFactory().getEditSession(new BukkitWorld(world), Integer.MAX_VALUE);
		CuboidClipboard cc = format.load(file);
		cc.paste(es, v, false);
		
		return true;
	}
	
	public static CuboidClipboard getSchematicIsland(String schem){
		File file = new File("plugins/SkyRealms/schematics/" + schem + ".schematic");
		SchematicFormat format = SchematicFormat.getFormat(file);
		if(format == null)
			return null;	
		CuboidClipboard cc = null;
		
		try {
			cc = format.load(file);
		} catch (DataException | IOException e) {
			e.printStackTrace();
		}
		return cc;
	}
	
	public static void buildSchematic(CuboidClipboard object, Location loc){
		Vector v = new Vector(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
		EditSession es = WorldEdit.getInstance().getEditSessionFactory().getEditSession(new BukkitWorld(loc.getWorld()), Integer.MAX_VALUE);
		
		try {
			object.paste(es, v, false);
		} catch (MaxChangedBlocksException e) {
			e.printStackTrace();
		}
	}
}
