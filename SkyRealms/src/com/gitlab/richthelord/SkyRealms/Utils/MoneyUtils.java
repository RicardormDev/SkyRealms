package com.gitlab.richthelord.SkyRealms.Utils;

import org.bukkit.entity.Player;

import com.gitlab.richthelord.SkyRealms.Main;

public class MoneyUtils {
	
	public static double getBalance(Player player){
		return Main.economy.getBalance(player);
	}
		
	public static void removeMoney(Player player, double cant){
		Main.economy.withdrawPlayer(player, cant);
	}
	
	public static void addMoney(Player player, double cant){
		Main.economy.depositPlayer(player, cant);
	}
	
}
