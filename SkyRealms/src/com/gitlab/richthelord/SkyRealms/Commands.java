package com.gitlab.richthelord.SkyRealms;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftVillager;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gitlab.richthelord.SkyRealms.IslandObject.Island;
import com.gitlab.richthelord.SkyRealms.IslandObject.IslandManager;
import com.gitlab.richthelord.SkyRealms.IslandObject.PrestigeIslands;
import com.gitlab.richthelord.SkyRealms.Utils.LocationUtils;
import com.gitlab.richthelord.SkyRealms.Utils.MoneyUtils;
import com.gitlab.richthelord.SkyRealms.bob.Menus.BobPrestige;
import com.gitlab.richthelord.SkyRealms.bob.Menus.BobPrincipal;
import com.gitlab.richthelord.SkyRealms.bob.NPC.Bob;
import com.gitlab.richthelord.SkyRealms.bob.NPC.CustomEntities;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_9_R1.EntityVillager;

public class Commands implements CommandExecutor{
	
	LocationUtils lu = new LocationUtils();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender instanceof Player){
			final Player player = (Player)sender;
			
			if(cmd.getName().equalsIgnoreCase("island") || cmd.getName().equalsIgnoreCase("is")){		
				if(args.length == 0){				
					Island Island = IslandManager.getInstance().getIsland(player.getName());
					if(Island == null){
						player.sendMessage(ChatColor.GREEN + "We are creating an island for you!");
						
						IslandManager.getInstance().createAnIsland(player);
						
						new Island(player.getName())
						.doPlayerActions(player)
						.build();
					} else {
						player.sendMessage(ChatColor.GREEN + "Commencing teleportation to your island");
						player.teleport(lu.deserializeLocation(SettingsManager.getIslands().<String>get("Islands." + player.getName() + ".center")));
					}
				} else if(args[0].equalsIgnoreCase("help")){
					//TODO: Help
				} else if(args[0].equalsIgnoreCase("go")){
					
					if(args.length > 1){
						Island Island = IslandManager.getInstance().getIsland(args[1]);
						
						if(Island == null){
							player.sendMessage(ChatColor.RED + "That island doesn't exist!");
							return true;
						}
						
						if(!Island.getInvitedPlayers().contains(player.getName())){
							player.sendMessage(ChatColor.RED + "You don't have acces to this island!");
							return true;
						}
						
						player.sendMessage(ChatColor.GREEN + "Commencing teleportation to the island of " + args[1]);
						player.teleport(lu.deserializeLocation(SettingsManager.getIslands().<String>get("Islands." + args[1] + ".center")));
						return true;
					}
					
					Island Island = IslandManager.getInstance().getIsland(player.getName());
					
					if(Island == null){
						player.sendMessage(ChatColor.RED + "You don't have any island");
						return true;
					}
					
					player.sendMessage(ChatColor.GREEN + "Commencing teleportation to your island");
					player.teleport(lu.deserializeLocation(SettingsManager.getIslands().<String>get("Islands." + player.getName() + ".center")));
				} else if(args[0].equalsIgnoreCase("bob")){
					new BobPrincipal().build(player);
				} else if(args[0].equalsIgnoreCase("reset")){
					final Island Island = IslandManager.getInstance().getIsland(player.getName());
					
					if(Island == null){
						player.sendMessage(ChatColor.RED + "You don't have any island");
						return true;
					}
					
					new ConfirmReset(player) {
						
						@Override
						public void action() {
							player.sendMessage(ChatColor.GREEN + "Reseting your island");
							Island c = IslandManager.getInstance().getIsland(player.getName());
							c.resetWithPrestige(c.getPrestige()).doPlayerActions(player);
						}
					}.open();
				} else if(args[0].equalsIgnoreCase("upgrade")){
					new BobPrestige(player).build(player);
				}} else if(args[0].equalsIgnoreCase("test")){
					Location a = player.getLocation().clone();
					a.add(60, 100, 60);
					Location b = player.getLocation().clone();
					b.subtract(60, 98, 60);
					
					if(args[1].equalsIgnoreCase("b")){
						CustomEntities.spawn(new Bob(player.getWorld()), player.getLocation());
						return true;
					}
					//CuboidRegion cr = new CuboidRegion(new Vector(a.getBlockX(), a.getBlockY(), a.getBlockZ()), new Vector(b.getBlockX(), b.getBlockY(), b.getBlockZ()));
					//CustomEntities.spawn(new Bob(player.getWorld()), player.getLocation());
					for(Entity en : lu.getNearbyEntities(player.getLocation(), Integer.parseInt(args[1]))){
						if(en instanceof Villager){
							EntityVillager znm = ((CraftVillager)en).getHandle();
							if(znm instanceof Bob){
								en.remove();
							}
						}		
					}
				} else if(args[0].equalsIgnoreCase("prestigereset")){
					if(player.isOp()){
						if(args.length < 3){
							player.sendMessage(ChatColor.RED + "Usage: /is prestigereset [player] [0,1,2,3,4,5]");
							return true;
						}
						Player target = Bukkit.getPlayer(args[1]);
						if(target == null){
							player.sendMessage(ChatColor.RED + "That player is not online");
							return true;
						}
						Island ti = IslandManager.getInstance().getIsland(target.getName());
						if(ti == null){
							player.sendMessage(ChatColor.RED + "That player does not have an island");
							return true;
						}
						String pn = args[2] + "0";
						int prestigeNum = Integer.parseInt(pn);			
						
						ti.resetWithPrestige(PrestigeIslands.getPrestige(prestigeNum)).doPlayerActions(player);
						
						player.sendMessage(ChatColor.GREEN + "Operation Complete");
					}
				} else if(args[0].equalsIgnoreCase("shop")){
					Main.shop.openShop(player);
				} else if(args[0].equalsIgnoreCase("t")){
					for(Island is : IslandManager.getInstance().getIslands()){
						player.sendMessage(is.getPlayer() + " - " + is.getCenter());
					}
				} else if(args[0].equalsIgnoreCase("getPortal")){
					Island is = IslandManager.getInstance().getIsland(player.getName());
					if(is == null){
						player.sendMessage(ChatColor.RED + "You don't have an island");
						return true;
					}
					
					if(MoneyUtils.getBalance(player) < 100){
						player.sendMessage(ChatColor.RED + "You  can't afford the portal, we're sorry");
						return true;
					}
					MoneyUtils.removeMoney(player, 100);				
					
					ItemStack item = new ItemStack(Material.OBSIDIAN);
					ItemMeta meta  = item.getItemMeta();
					meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Island Portal | Invite people :D");
					meta.setLore(Arrays.asList(ChatColor.GRAY + "Place this block on the spawn of the server"));
					item.setItemMeta(meta);
					
					player.getInventory().addItem(item);
				} else if(args[0].equalsIgnoreCase("add")){
					Island is = IslandManager.getInstance().getIsland(player.getName());
					if(is == null){
						player.sendMessage(ChatColor.RED + "You don't have an island");
						return true;
					}
					
					if(args.length < 2){
						player.sendMessage(ChatColor.RED + "Usage: /is add [player]");
						return true;
					}
					
					is.addPlayer(args[1]);
					player.sendMessage(ChatColor.GREEN + "You've added the player " + args[1] + " to your players list!");
				} else if(args[0].equalsIgnoreCase("remove")){
					Island is = IslandManager.getInstance().getIsland(player.getName());
					if(is == null){
						player.sendMessage(ChatColor.RED + "You don't have an island");
						return true;
					}
					
					if(args.length < 2){
						player.sendMessage(ChatColor.RED + "Usage: /is remove [player]");
						return true;
					}
					
					is.removePlayer(args[1]);
					player.sendMessage(ChatColor.GREEN + "You removed " + args[1] + " from your island");
				} else if(args[0].equalsIgnoreCase("playersList")){
					Island is = IslandManager.getInstance().getIsland(player.getName());
					if(is == null){
						player.sendMessage(ChatColor.RED + "You don't have an island");
						return true;
					}
					player.sendMessage(ChatColor.GREEN + "Only this players have acces to your island:");
					for(String str : is.getInvitedPlayers()){
						player.sendMessage(ChatColor.GOLD + " - " + str);
					}
				} else if(args[0].equalsIgnoreCase("fixNPC")){
					Island is = IslandManager.getInstance().getIsland(player.getName());
					if(is == null){
						player.sendMessage(ChatColor.RED + "You don't have an island");
						return true;
					}
					is.fixNPC();
					player.sendMessage(ChatColor.GREEN + "You've fixed your NPC");
				}
				return true;
			}

		
		if(cmd.getName().equalsIgnoreCase("sell")){
			if(sender.isOp()){
				// /sell [item] [cant] [prize]
				
				if(args.length < 4){
					sender.sendMessage("USAGE: /sell [item] [cant] [prize] [player]");
					return true;
				}
				
				Player p = null;
				p = Bukkit.getServer().getPlayer(args[3]);
				
				Inventory inv = p.getInventory();
				@SuppressWarnings("deprecation")
				Material mat = Material.getMaterial(Integer.parseInt(args[0]));
				int    prize = Integer.parseInt(args[2]);
				int     cant = Integer.parseInt(args[1]);
				boolean cont = false;
				String error = "";

				if(!(inv.contains(mat))){
					p.sendMessage(ChatColor.RED + "You don't have that item!");
					return true;
				} else {
					for(ItemStack is : p.getInventory().all(mat).values()){
						if(is.getAmount() < cant){
							cont = false;
							error = "&4You need more items!";
						} else if(is.getAmount() == cant){
							cont = true;
							inv.remove(is);
						} else {
							cont = true;
							is.setAmount(is.getAmount() - cant);
						}
					}
				}
				
				
				
				if(cont){
					
					MoneyUtils.addMoney(p, prize);
					
					p.sendMessage(ChatColor.GREEN + "You sold " + cant + " "  + mat.toString().toLowerCase() + " for $" + prize + "!");
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes("&".charAt(0), error));
				}
				
				
			}
		}
		
		return true;
	}

}
